# @manuscripts/plugin-persist

This repo provides a plugin that automatically adds ID attributes to Nodes that need them, to facilitate persistent storage in Pouch/Couchbase.

## Usage

Install:

```sh
npm i @manuscripts/plugin-persist
```

Add to Editor on setup

```js
import persist from @manuscripts/plugin-persist

EditorState.create({
  doc,
  schema,
  plugins: [persist()],
})
```

## Development

```sh
git clone git@gitlab.com:mpapp-public/manuscripts-plugin-persist.git
cd manuscripts-plugin-persist
npm i
npm run typecheck
npm run lint
npm run build
```
